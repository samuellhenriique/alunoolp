object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 180
  ClientWidth = 327
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Abrir: TButton
    Left = 4
    Top = 134
    Width = 75
    Height = 41
    Caption = 'Abrir Tabela'
    TabOrder = 0
    OnClick = AbrirClick
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 8
    Width = 320
    Height = 120
    DataSource = DataModule2.DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'nome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'matricula'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'sexo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cpf'
        Visible = True
      end>
  end
  object ButtonInserir: TButton
    Left = 120
    Top = 134
    Width = 89
    Height = 41
    Caption = 'Inserir'
    TabOrder = 2
    OnClick = ButtonInserirClick
  end
  object ButtonDelete: TButton
    Left = 249
    Top = 134
    Width = 75
    Height = 41
    Caption = 'Deletar'
    TabOrder = 3
    OnClick = ButtonDeleteClick
  end
end
